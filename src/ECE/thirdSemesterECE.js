import React, { Component } from 'react';
import SecondSemesterECE from './secondSemesterECE.js'
import FourthSemesterECE from './fourthSemesterECE.js'


class ThirdSemesterECE extends Component {
    constructor(props) {
      super(props);
      this.state = {
        subjectsDetails: {items: {
            subjectsList: [{subjectName: "Linear Algebra and Partial Differential Equations", subjectCode: "MA8352", subjectCategory: "Theoretical Subject"},
            {subjectName: "Fundamentals of Data Structures in C", subjectCode: "EC8393", subjectCategory: "Theoretical Subject"},
            {subjectName: "Electronic Circuits-I", subjectCode: "EC8351", subjectCategory: "Theoretical Subject"}, 
            {subjectName: "Signals and Systems", subjectCode: "EC8352", subjectCategory: "Theoretical Subject"}, 
            {subjectName: "Digital Electronics", subjectCode: "EC8392", subjectCategory: "Theoretical Subject"}, 
            {subjectName: "Control Systems Engineering", subjectCode: "EC8391", subjectCategory: "Theoretical Subject"},
            {subjectName: "Fundamentals of Data Structures in C Laboratory", subjectCode: "EC8381", subjectCategory: "Practical Subject"},
            {subjectName: "Analog and Digital Circuits Laboratory", subjectCode: "EC8361", subjectCategory: "Practical Subject"},
            {subjectName: "Interpersonal Skills/Listening & Speaking", subjectCode: "HS8381", subjectCategory: "Practical Subject"}],
            creditsAlloted: [{credits: 4}, {credits: 3}, {credits: 3}, {credits: 4}, {credits: 3}, {credits: 3}, {credits: 2}, {credits: 2}, {credits: 1}],
            selectedGradePoints: [{selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""},{selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}],
            gradeLists: [{grade: "O", gradePoint: 10}, {grade: "A+", gradePoint: 9}, {grade: "A", gradePoint: 8}, {grade: "B+", gradePoint: 7}, {grade: "B", gradePoint: 6}, {grade: "RA", gradePoint: 0}, {grade: "SA", gradePoint: 0}, {grade: "W", gradePoint: 0}] },
    },
gradePointAverage: "",
summationGradePoints: 0,
sumofcredits: 25,
secondSemesterECE: false,
thirdSemesterECE: false,
}
}

calculateGPA() {
    this.state.summationGradePoints = 0;
    for(var i=0; i<9; i++) {
      this.state.summationGradePoints = (this.state.summationGradePoints + (this.state.subjectsDetails.items.selectedGradePoints[i].selectedGradePoint * this.state.subjectsDetails.items.creditsAlloted[i].credits));
      this.setState({summationGradePoints: this.state.summationGradePoints});
      console.log(this.state.summationGradePoints);
    }
    return this.state.summationGradePoints;
  }

  showSubjectCategoryLabel(subjectcategory, index){
    if((subjectcategory === "Theoretical Subject") && (index === 0)) {
        return "block";
    }
    if((subjectcategory === "Practical Subject") && (index === 6)) {
      return "block";
    }
    return "none";
  } 
    
     
    render() {
        return(
        (this.state.secondSemesterECE) ? <SecondSemesterECE/> : 
        (this.state.fourthSemesterECE) ? <FourthSemesterECE/> : 
        <div>
        {this.state.subjectsDetails.items.subjectsList.map((subjects, index) => {
            return(
            <tr>
            <td>
            <label style={{display: this.showSubjectCategoryLabel(subjects.subjectCategory, index)}}>{subjects.subjectCategory}</label>
            <label>{subjects.subjectCode}</label>
            <label>{subjects.subjectName}</label>
            <select onClick={(event)=>{this.state.subjectsDetails.items.selectedGradePoints[index].selectedGradePoint = event.target.value; }}>
              <option value="" >Select the Grade</option>
              {this.state.subjectsDetails.items.gradeLists.map((grades,index1) => {
              return(
              <option value={grades.gradePoint}>{grades.grade}</option>
              )})}
            </select>
            </td>
            </tr>
            )
            }) }
            <button type="button" onClick={(event)=>{ this.state.gradePointAverage = this.calculateGPA()/this.state.sumofcredits; this.setState({gradePointAverage: this.state.gradePointAverage}); }}>GPA</button>
            <input type="text" value={this.state.gradePointAverage} />
            <br></br>
            <button type="button" onClick={(event)=>{this.setState({secondSemesterECE: true})}}>BACK</button>
            <br></br>
            <button type="button" onClick={(event)=>{this.setState({thirdSemesterECE: true})}}>NEXT</button>
          </div>
        );
      }
}   
      
export default ThirdSemesterECE;
