import React from 'react';
import ReactDOM from 'react-dom';
// import FirstSemesterECE from 'ECE/firstSemesterECE.js';
import DepartmentSelection from './admin.js';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DepartmentSelection />, div);
  ReactDOM.unmountComponentAtNode(div);
});
