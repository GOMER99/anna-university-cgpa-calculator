import React, { Component } from 'react';
import './App.css';
import FirstSemesterECE from './ECE/firstSemesterECE';


class DepartmentSelection extends Component {
    constructor(props) {
      super(props);
      this.state = {
          selectedECEDepartment: false,
          departments: [
                            {department:"ECE"},
                            {department:"CSE"},
                            {department:"EEE"},
                            {department:"IT"},
                            {department:"MECH"},
                            {department: "CIVIL"}
                        ]
      }
    
    }
    
    render() {
        return(            
            <div className="container">
                {(this.state.selectedECEDepartment) ? <FirstSemesterECE/> :
                <div className="row">
                    <div className="col-md-12">
                        <h1>Anna University CGPA Calculator</h1>
                            <div className="border"> 
                                <div className="col-md-3 d-flex">
                                {this.state.departments.map((department)=>{ 
                                    return(
                                            <div className="borderinside ">
                                                <input type="radio" id={department.department} value={department.department} disabled={false} onClick={(event)=>{ if(event.target.value === "ECE") { console.log("ECE Clicked"); this.setState({selectedECEDepartment: true}); }}} />{department.department} 
                                            </div>
                                    ) 
                                })} 
                                </div>                       
                            {/* {(this.state.selectedECEDepartment) ? <FirstSemesterECE/> : 
                                <div>
                                    <input type="radio" name="ECE" value="ece" onClick={(event)=>{this.setState({selectedECEDepartment: event.target.checked})}} />ECE
                                </div>
                            }   */}
                            </div>                          
                    </div>
                </div>
                }
            </div>
            
        )
            
    }
}

export default DepartmentSelection;
        

       