import React from 'react';
import ReactDOM from 'react-dom';
import { AppProvider } from './provider.js';
// import { ToastContainer } from 'react-toastify';
import './index.css';
import DepartmentSelection from './admin.js';
import * as serviceWorker from './serviceWorker.js';

document.addEventListener('DOMContentLoaded', function() {
ReactDOM.render(
    <div>
    {/* <ToastContainer
        position='top-right'
        autoClose={4000}
        hideProgressBar
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnVisibilityChange
        draggable
        pauseOnHover
    /> */}
    <AppProvider><DepartmentSelection /></AppProvider> 
</div>,
    document.getElementById('root'));
    serviceWorker.unregister();
});




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
