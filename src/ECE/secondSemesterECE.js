import React, { Component } from 'react';
import FirstSemesterECE from './firstSemesterECE.js'
import ThirdSemesterECE from './thirdSemesterECE.js'


class SecondSemesterECE extends Component {
    constructor(props) {
      super(props);
      this.state = {
        subjectsDetails: {items: {
            subjectsList: [{subjectName: "Technical English", subjectCode: "HS8251", subjectCategory: "Theoretical Subject"},
            {subjectName: "Engineering Mathematics - II", subjectCode: "MA8251", subjectCategory: "Theoretical Subject"},
            {subjectName: "Physics for Electronics Engineering", subjectCode: "PH8253", subjectCategory: "Theoretical Subject"}, 
            {subjectName: "Basic Electrical and Instrumentation Engineering", subjectCode: "BE8254", subjectCategory: "Theoretical Subject"}, 
            {subjectName: "Circuit Analysis", subjectCode: "EC8251", subjectCategory: "Theoretical Subject"}, 
            {subjectName: "Electronic Devices", subjectCode: "EC8252", subjectCategory: "Theoretical Subject"},
            {subjectName: "Circuits and Devices Laboratory", subjectCode: "EC8261", subjectCategory: "Practical Subject"},
            {subjectName: "Engineering Practices Laboratory", subjectCode: "GE8261", subjectCategory: "Practical Subject"}],
            creditsAlloted: [{credits: 4}, {credits: 4}, {credits: 3}, {credits: 3}, {credits: 4}, {credits: 3}, {credits: 2}, {credits: 2}],
            selectedGradePoints: [{selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""},{selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}],
            gradeLists: [{grade: "O", gradePoint: 10}, {grade: "A+", gradePoint: 9}, {grade: "A", gradePoint: 8}, {grade: "B+", gradePoint: 7}, {grade: "B", gradePoint: 6}, {grade: "RA", gradePoint: 0}, {grade: "SA", gradePoint: 0}, {grade: "W", gradePoint: 0}] },
    },
gradePointAverage: "",
summationGradePoints: 0,
sumofcredits: 25,
firstSemesterECE: false,
thirdSemesterECE: false,
}
}

calculateGPA() {
    this.state.summationGradePoints = 0;
    for(var i=0; i<8; i++) {
      this.state.summationGradePoints = (this.state.summationGradePoints + (this.state.subjectsDetails.items.selectedGradePoints[i].selectedGradePoint * this.state.subjectsDetails.items.creditsAlloted[i].credits));
      this.setState({summationGradePoints: this.state.summationGradePoints});
      console.log(this.state.summationGradePoints);
    }
    return this.state.summationGradePoints;
  }

  showSubjectCategoryLabel(subjectcategory, index){
    if((subjectcategory === "Theoretical Subject") && (index == 0)) {
        return "block";
    }
    if((subjectcategory === "Practical Subject") && (index == 6)) {
      return "block";
    }
    return "none";
  } 
    
     
    render() {
        return(
        (this.state.firstSemesterECE) ? <FirstSemesterECE/> : 
        (this.state.thirdSemesterECE) ? <ThirdSemesterECE/> : 
        <div>

        {this.state.subjectsDetails.items.subjectsList.map((subjects, index) => {
            return(
            <tr>
            <td>
            <label style={{display: this.showSubjectCategoryLabel(subjects.subjectCategory, index)}}>{subjects.subjectCategory}</label>
            <label>{subjects.subjectCode}</label>
            <label>{subjects.subjectName}</label>
            <select onClick={(event)=>{this.state.subjectsDetails.items.selectedGradePoints[index].selectedGradePoint = event.target.value; }}>
              <option value="" >Select the Grade</option>
              {this.state.subjectsDetails.items.gradeLists.map((grades,index1) => {
              return(
              <option value={grades.gradePoint}>{grades.grade}</option>
              )})}
            </select>
            </td>
            </tr>
            )
            }) }
            <button type="button" onClick={(event)=>{ this.state.gradePointAverage = this.calculateGPA()/this.state.sumofcredits; this.setState({gradePointAverage: this.state.gradePointAverage}); }}>GPA</button>
            <input type="text" value={this.state.gradePointAverage} />
            <br></br>
            <button type="button" onClick={(event)=>{this.setState({firstSemesterECE: true})}}>BACK</button>
            <br></br>
            <button type="button" onClick={(event)=>{this.setState({thirdSemesterECE: true})}}>NEXT</button>
          </div>
        );
      }
}   
      
export default SecondSemesterECE;





