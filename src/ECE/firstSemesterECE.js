import React, { Component } from 'react';
import SecondSemesterECE from './secondSemesterECE.js'
import {AppContext} from '../context.js'
//import logo from './logo.svg';
// import '../App.css';
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';

class FirstSemesterECE extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subjectsDetails: {items: {
                                subjectsList: [{subjectName: "Communicative English", subjectCode: "HS8151", subjectCategory: "Theoretical Subject"},
                                               {subjectName: "Engineering Mathematics - I", subjectCode: "MA8151", subjectCategory: "Theoretical Subject"},
                                               {subjectName: "Engineering Physics", subjectCode: "PH8151", subjectCategory: "Theoretical Subject"}, 
                                               {subjectName: "Engineering Chemistry", subjectCode: "CY8151", subjectCategory: "Theoretical Subject"}, 
                                               {subjectName: "Problem Solving and Python Programming", subjectCode: "GE8151", subjectCategory: "Theoretical Subject"}, 
                                               {subjectName: "Engineering Graphics", subjectCode: "GE8152", subjectCategory: "Theoretical Subject"}, 
                                               {subjectName: "Problem Solving and Python Programming Laboratory", subjectCode: "GE8161", subjectCategory: "Practical Subject"}, 
                                               {subjectName: "Physics and Chemistry Laboratory", subjectCode: "GE8152", subjectCategory: "Practical Subject"}],
                                creditsAlloted: [{credits: 4}, {credits: 4}, {credits: 4}, {credits: 3}, {credits: 3}, {credits: 3}, {credits: 2}, {credits: 2}],
                                selectedGradePoints: [{selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""}, {selectedGradePoint: ""},{selectedGradePoint: ""},{selectedGradePoint: ""},{selectedGradePoint: ""}],
                                gradeLists: [{grade: "O", gradePoint: 10}, {grade: "A+", gradePoint: 9}, {grade: "A", gradePoint: 8}, {grade: "B+", gradePoint: 7}, {grade: "B", gradePoint: 6}, {grade: "RA", gradePoint: 0}, {grade: "SA", gradePoint: 0}, {grade: "W", gradePoint: 0}] },
                        },
      gradePointAverage: "",
      summationGradePoints: 0,
      sumofcredits: 25,
      secondSemesterECE: false,
    }
      
  }

  calculateGPA() {
      this.state.summationGradePoints = 0;
      for(var i=0; i<8; i++) {
        this.state.summationGradePoints = (this.state.summationGradePoints + (this.state.subjectsDetails.items.selectedGradePoints[i].selectedGradePoint * this.state.subjectsDetails.items.creditsAlloted[i].credits));
        this.setState({summationGradePoints: this.state.summationGradePoints});
        console.log(this.state.summationGradePoints);
      }
      return this.state.summationGradePoints;
    }

    showSubjectCategoryLabel(subjectcategory, index){
        if((subjectcategory === "Theoretical Subject") && (index == 0)) {
            return "block";
        }
        if((subjectcategory === "Practical Subject") && (index == 6)) {
          return "block";
        }
        return "none";
    }
  
    
  
  render() {
  return (
    (this.state.secondSemesterECE) ? <SecondSemesterECE/> : 
    <div>
      {this.state.subjectsDetails.items.subjectsList.map((subjects, index) => {
      return(
      <tr>
      <td>
      <label style={{display: this.showSubjectCategoryLabel(subjects.subjectCategory, index)}}>{subjects.subjectCategory}</label>
      <label>{subjects.subjectCode}</label>
      <label>{subjects.subjectName}</label>
      <select onClick={(event)=>{this.state.subjectsDetails.items.selectedGradePoints[index].selectedGradePoint = event.target.value; }}>
        <option value="" >Select the Grade</option>
        {this.state.subjectsDetails.items.gradeLists.map((grades,index1) => {
        return(
        <option value={grades.gradePoint}>{grades.grade}</option>
        )})}
      </select>
      </td>
      </tr>
      )
      }) }
      <button className="btn btn-primary" type="button" onClick={(event)=>{ this.state.gradePointAverage = this.calculateGPA()/this.state.sumofcredits; this.setState({gradePointAverage: this.state.gradePointAverage}); }}>GPA</button>
      <input type="text" value={this.state.gradePointAverage} />
      <br></br>
      <button type="button" onClick={(event)=>{ this.state.gradePointAverage = this.calculateGPA()/this.state.sumofcredits; this.setState({gradePointAverage: this.state.gradePointAverage}); }}>CGPA</button>
      <input type="text" value={this.state.gradePointAverage} />
      <br></br>
      <button type="button" >BACK</button>
      <br></br>
      <button type="button" onClick={(event)=>{this.setState({secondSemesterECE: true})}}>NEXT</button>
    </div>
  );
}
}


export default FirstSemesterECE;
